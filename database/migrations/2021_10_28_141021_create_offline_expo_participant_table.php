<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfflineExpoParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offline_expo_participant', function (Blueprint $table) {
            $table->increments('off_expo_participant_id');
            $table->foreignId('off_expo_id');
            $table->foreignId('user_id');
            $table->boolean('is_present')->default(0);
            $table->string('qr_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offline_expo_participant');
    }
}
