<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Jenssegers\Agent\Agent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $agent = new Agent();

    if ($agent->isPhone()) return view('mobiles/welcome');
    else if ($agent->isTablet()) return view('tablet/welcome');
    else return view('desktop/welcome');
})->name('welcome');

// Home
Route::group(['middleware' => ['auth', 'role:admin|moderator|user'], 'prefix' => 'home'], function () {
    Route::get('/', App\Http\Livewire\Home\HomeLivewire::class)->name('home');
    Route::get('validVisitor', [App\Http\Livewire\Home\HomeLivewire::class, 'validVisitor'])->name('validVisitor');
    Route::get('validVisitorExpo', [App\Http\Livewire\Home\HomeLivewire::class, 'validVisitorExpo'])->name('validVisitorExpo');
    Route::get('your-qr', [App\Http\Livewire\Home\HomeLivewire::class, 'yourqr'])->name('your-qr');
    Route::get('generateNewQr', [App\Http\Livewire\Home\HomeLivewire::class, 'generateNewQr'])->name('generateNewQr');
    Route::get('goToExpo', [App\Http\Livewire\Home\HomeLivewire::class, 'goToExpo'])->name('goToExpo');

    Route::get('locale/{param}', function (Request $request) {
        $storagePath = public_path('/cop_render/locale/' . $request->param);
        return response()->file($storagePath);
    });

    Route::get('lib/cursors/{param}', function (Request $request) {
        $storagePath = public_path('/cop_render/lib/cursors/' . $request->param);
        return response()->file($storagePath);
    });
});

Route::group(['middleware' => ['auth', 'role:admin|moderator|user'], 'prefix' => 'event'], function () {
    Route::get('/events', App\Http\Livewire\Event\EventLivewire::class)->name('events');
    Route::get('/expo', App\Http\Livewire\Event\OffExpoLivewire::class)->name('expo');

    Route::get('/role', App\Http\Livewire\RoleLivewire::class)->name('role');
    Route::get('/user', App\Http\Livewire\UserLivewire::class)->name('user');
});


// Example
Route::group(['middleware' => ['auth', 'role:admin|moderator|user'], 'prefix' => 'example'], function () {
    Route::get('crud', App\Http\Livewire\Example\CRUDLivewire::class)->name('example.crud');
});

Route::prefix('/booth')->name('booth.')->group(function () {
    Route::get('/done', [App\Http\Controllers\BoothController::class, 'doneVisit'])->name('visit.done');

    Route::get('/{slug}', [App\Http\Controllers\BoothController::class, 'showVisit'])->name('visit');
    Route::post('/{slug}', [App\Http\Controllers\BoothController::class, 'visit']);

    Route::get('/{slug}/product', [App\Http\Controllers\BoothController::class, 'product'])->name('product');
});

Route::get('/unauthorized', function () {
    return view('unauthorized');
})->name('unauthorized');
