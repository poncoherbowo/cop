<?php

if (!function_exists('setTableOrder')) {
    function setTableOrder($orders, $col)
    {
        if(!isset($orders[$col])){
            $orders[$col] = "asc";
        } else {
            if($orders[$col] == "asc"){
                $orders[$col] = "desc";
            } else if($orders[$col] == "desc"){
                unset($orders[$col]);
            }
        }
        return $orders;
    }
}