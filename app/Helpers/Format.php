<?php

use Carbon\Carbon;
if (!function_exists('toDate')) {
    function toDate($datetime, $format = 'date')
    {
        if($datetime == null) return null;
        
        if($format == 'datetime'){
            $format = 'd M Y H:m';
        } else if($format == 'date'){
            $format = 'd M Y';
        }
        return Carbon::parse($datetime)->format($format);
    }
}
if (!function_exists('toSystemDate')) {
    function toSystemDate($datetime)
    {
        $datetime = str_replace("/", "-", $datetime);
        return Carbon::parse($datetime);
    }
}

if (!function_exists('toNumber')) {
    function toNumber($double, $decimal = 0, $dec_point = ",")
    {
        if($double == '' || $double == null) return 0;

        $thousand_sep = $dec_point == "," ? "." : ",";
        return number_format($double, $decimal, $dec_point, $thousand_sep);
    }
}

if (!function_exists('toSystemNumber')) {
    function toSystemNumber($string)
    {
        if($string == '' || $string == null) return 0;

        return str_replace('.', '', str_replace(',', '.', $string));
    }
}

if (!function_exists('toObject')){
    function toObject($array){
        $json = json_encode($array);
        $object = json_decode($json);
        return $object;
    }
}

if (!function_exists('toArray')){
    function toArray($obj){
        if (!is_object($obj) && !is_array($obj)) {
            return $obj;
        }
        foreach ($obj as $key => $value) {
            $arr[$key] = toArray($value);
        }
        return $arr;
    }
}