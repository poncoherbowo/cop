<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use PHPQRCode\QRcode;

// use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        // $path = storage_path() . '/app/public/qr';

        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        $data =  User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ])->assignRole('user');

        // $fileQr = $path . '/' . $data->id . '.png';
        User::where([
            'id' => $data->id
        ])->update([
            'phone' => $input['phone'],
            'country' => $input['country'],
            // 'qr_path' => 'qr/'. $data->id . '.png'
        ]);

        // if (!File::isDirectory($path)) {
        //     File::makeDirectory($path, 0777, true);
        // }
        // // QrCode::size(100)->generate($data->id, $fileQr);

        // $qrText = (string) $data->id;
        // QRcode::png($qrText,$fileQr,'H',10,10);
        // chmod($fileQr,0777);

        // Mail::send('mail', [
        //     'id' => $data->id,
        //     'name' => $input['name'],
        //     'phone' => $input['phone'],
        // ], function ($message) use ($input, $fileQr) {
        //     $message->to($input['email'], $input['name'])->subject('Registration COP 2021');
        //     $message->from('no-reply@cop26virtualexpo2021.com', 'COP 2021');
        //     $message->attach($fileQr);
        // });

        return $data;
    }
}
