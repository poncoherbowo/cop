<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booth extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug',
        'url',
        'visitor_count'
    ];

    protected $dates = ['deleted_at'];

    public function visitors()
    {
        return $this->hasMany('Modules\Booth\Models\BoothVisitor', 'booth_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('Modules\Booth\Models\BoothProduct', 'booth_id', 'id');
    }
}
