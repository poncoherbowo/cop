<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfflineExpoParticipant extends Model
{
    /*
    || Load 'Eloquent' addon
    */
    use HasFactory;

    /*
    || Table info
    */
    protected $table = 'offline_expo_participant';
    protected $primaryKey = 'off_expo_id';

    /*
    || Table fillable
    */
    protected $fillable = [
        'off_expo_id', 'user_id', 'is_present', 'qr_path'
    ];

    /*
    || Relationship
    */
    public function event()
    {
        return $this->belongsTo(OfflineExpoParticipant::class, 'off_expo_id', 'off_expo_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
