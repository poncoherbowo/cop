<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /*
    || Load 'Eloquent' addon
    */
    use HasFactory;

    /*
    || Table info
    */
    protected $table = 'roles';
    protected $primaryKey = 'id';

    /*
    || Table fillable
    */
    protected $fillable = [
        'name',
        'guard_name',
    ];

    
}
