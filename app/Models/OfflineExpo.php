<?php

namespace App\Models;

use CreateOfflineExpoParticipantTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfflineExpo extends Model
{
    /*
    || Load 'Eloquent' addon
    */
    use HasFactory;

    /*
    || Table info
    */
    protected $table = 'offline_expo';
    protected $primaryKey = 'off_expo_id';

    /*
    || Table fillable
    */
    protected $fillable = [
        'off_expo_name',
        'held_on',
        'is_active',
        'is_online',
    ];

    /*
    || Relationship
    */
    public function participant()
    {
        return $this->hasMany(OfflineExpoParticipant::class, 'off_expo_id', 'off_expo_id');
    }
}
