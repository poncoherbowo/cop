<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoothProduct extends Model
{
    protected $fillable = [
        'booth_id',
        'product',
        'url',
    ];

    public function booth()
    {
        return $this->belongsTo('Modules\Booth\Models\Booth', 'booth_id', 'id');
    }
}
