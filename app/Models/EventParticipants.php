<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventParticipants extends Model{
    /*
    || Load 'Eloquent' addon
    */
    use HasFactory;

    /*
    || Table info
    */
    protected $table = 'event_participants';
    protected $primaryKey = 'participant_id';

    /*
    || Table fillable
    */
    protected $fillable = [
        'event_id', 'user_id', 'is_present','qr_path'
    ];

    /*
    || Relationship
    */
    public function event(){
        return $this->belongsTo(Events::class, 'event_id', 'event_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
