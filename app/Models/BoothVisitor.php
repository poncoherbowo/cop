<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoothVisitor extends Model
{
    protected $fillable = [
        'booth_id',
        'user_id',
        'name',
        'email',
        'phone',
    ];

    public function booth()
    {
        return $this->belongsTo('Modules\Booth\Models\Booth', 'booth_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Models\User', 'user_id', 'id')->withTrashed();
    }
}
