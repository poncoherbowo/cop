<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model{
    /*
    || Load 'Eloquent' addon
    */
    use HasFactory;

    /*
    || Table info
    */
    protected $table = 'events';
    protected $primaryKey = 'event_id';

    /*
    || Table fillable
    */
    protected $fillable = [
        'event_name',
        'url',
        'held_on',
        'is_active',
        'is_online',
    ];

    /*
    || Relationship
    */
    public function participant(){
        return $this->hasMany(EventParticipants::class, 'event_id', 'event_id');
    }
    
}
