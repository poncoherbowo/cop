<?php

namespace App\Exports;

use App\Models\OfflineExpoParticipant;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExpoParticipantsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(
            OfflineExpoParticipant::select(
                'offline_expo_participant.off_expo_participant_id',
                'offline_expo_participant.off_expo_id',
                'offline_expo.off_expo_name',
                'offline_expo_participant.user_id',
                'users.name',
                'users.email',
                'users.phone',
                DB::raw('(CASE WHEN offline_expo_participant.is_present = 1 THEN "Yes" ELSE "No" END) AS is_present_label'),
                'offline_expo_participant.created_at',
                'offline_expo_participant.updated_at',
            )
            ->join('offline_expo', ['offline_expo.off_expo_id' => 'offline_expo_participant.off_expo_id'])
            ->join('users', ['users.id' => 'offline_expo_participant.user_id'])
            ->get()
        );
        
    }

    public function headings(): array
    {
        return [
            'participant_id',
            'off_expo_id',
            'off_expo_name',
            'user_id',
            'user_name',
            'user_email',
            'user_phone',
            'is_present',
            'created_at',
            'updated_at',
        ];
    }
}