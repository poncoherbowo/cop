<?php

namespace App\Exports;

use App\Models\EventParticipants;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EventParticipantsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(
            EventParticipants::select(
                'event_participants.participant_id',
                'event_participants.event_id',
                'events.event_name',
                'event_participants.user_id',
                'users.name',
                'users.email',
                'users.phone',
                DB::raw('(CASE WHEN event_participants.is_present = 1 THEN "Yes" ELSE "No" END) AS is_present_label'),
                'event_participants.created_at',
                'event_participants.updated_at',
            )
            ->join('events', ['events.event_id' => 'event_participants.event_id'])
            ->join('users', ['users.id' => 'event_participants.user_id'])
            ->get()
        );
        
    }

    public function headings(): array
    {
        return [
            'participant_id',
            'event_id',
            'event_name',
            'user_id',
            'user_name',
            'user_email',
            'user_phone',
            'is_present',
            'created_at',
            'updated_at',
        ];
    }
}