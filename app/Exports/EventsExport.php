<?php

namespace App\Exports;

use App\Models\Events;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EventsExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect(
            Events::select(
                'event_id',
                'event_name',
                'url',
                'held_on',
                DB::raw('(CASE WHEN events.is_active = 1 THEN "Yes" ELSE "No" END) AS is_active_label'),
                DB::raw('(CASE WHEN events.is_online = 1 THEN "Yes" ELSE "No" END) AS is_online_label'),
                'created_at',
                'updated_at'
            )->get()
        );

        // $select = [
        //     'events.event_id',
        //     'events.event_name',
        //     'events.url',
        //     'events.held_on',
        //     // 'events.is_active',
        //     // 'events.is_online',
        //     'events.created_at',
        //     'events.updated_at',
        // ];

        // $select[] = DB::raw('(CASE WHEN events.is_active = 1 THEN "Yes" ELSE "No" END) AS is_active_label');
        // $select[] = DB::raw('(CASE WHEN events.is_online = 1 THEN "Yes" ELSE "No" END) AS is_online_label');

        // return collect(DB::table('events')
        //             ->select($select)
        //             ->get());
    }

    public function headings(): array
    {
        return [
            'event_id',
            'event_name',
            'url',
            'held_on',
            'is_active',
            'is_online',
            'created_at',
            'updated_at',
        ];
    }
}
