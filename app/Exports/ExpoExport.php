<?php

namespace App\Exports;

use App\Models\OfflineExpo;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExpoExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(
            OfflineExpo::select(
                'off_expo_id',
                'off_expo_name',
                'held_on',
                DB::raw('(CASE WHEN offline_expo.is_active = 1 THEN "Yes" ELSE "No" END) AS is_active_label'),
                'created_at',
                'updated_at',
            )->get()
        );
        
    }

    public function headings(): array
    {
        return [
            'off_expo_id',
            'off_expo_name',
            'held_on',
            'is_active',
            'created_at',
            'updated_at',
        ];
    }
}
