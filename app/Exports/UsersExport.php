<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(
            User::select(
                'users.id',
                'users.name',
                'users.email',
                'users.phone',
                'roles.name as roles',
                'users.created_at',
                'users.updated_at',
            )
            ->join('model_has_roles', ['model_has_roles.model_id' => 'users.id'])
            ->join('roles', ['roles.id' => 'model_has_roles.role_id'])
            ->get()
        );
        
    }

    public function headings(): array
    {
        return [
            'id',
            'user_name',
            'user_email',
            'user_phone',
            'role',
            'created_at',
            'updated_at',
        ];
    }
}
