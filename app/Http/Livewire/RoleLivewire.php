<?php

namespace App\Http\Livewire;

use App\Exports\RoleExport;
use App\Models\Role;
use Livewire\Component;
use Livewire\WithPagination;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use Maatwebsite\Excel\Facades\Excel;

class RoleLivewire extends Component
{
    // Load addon trait
    use WithPagination, LivewireAlert;

    // Bootsrap pagination
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpen = 0;
    public $paginatedPerPages = 10;
    public $post_id, $searchTerm, $name, $guard_name;

    // View extending the App Layouts, so we won't do this on Blade
    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.role.role', [
            'lists' =>
            Role::where([
                ['name', 'like', '%' . $searchData . '%'],
            ])->paginate($this->paginatedPerPages),

        ])->extends('layouts.app');
    }

    // Reset input fields
    private function resetInputFields()
    {
        $this->reset([
            'post_id', 'name', 'guard_name'
        ]);
    }

    // Open input form
    public function openModal()
    {
        $this->isOpen = true;
    }

    // Close input form
    public function closeModal()
    {
        $this->isOpen = false;
    }

    // Open input form and then reset input fields
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
    }

    public function store()
    {
        // Send a custom message if something is error
        $messages = [
            '*.required'                => 'This column is required',
            '*.numeric'                 => 'This column is required to be filled in with number',
            '*.string'                  => 'This column is required to be filled in with letters',
        ];

        // Validate input with custom message
        $this->validate([
            'name' => ['required'],
            'guard_name' => ['required'],
        ], $messages); // Delete this '$messages' variable if you don't want to use the custom message validator

        // Photo Name with Regex - Replace anything weird with underscore


        // Upload Photo if this is a 'Create'
        if ($this->post_id == false) {
        }

        // Delete Existing Photo and then Upload the New One if this is an 'Update'
        elseif ($this->post_id == true) {
        }

        // Insert or Update if Ok
        Role::updateOrCreate(['id' => $this->post_id], [
            'name' => $this->name,
            'guard_name' => $this->guard_name,
        ]);

        // Show an alert
        $this->alert('success', $this->post_id ? 'Data Has Been Updated Successfully!' : 'Data Has Been Submited Successfully!');

        // Close input form, we're going back to the list
        $this->closeModal();

        // Reset input fields for next input
        $this->resetInputFields();
    }

    public function edit($id)
    {
        // Find data from the $id
        $post = Role::findOrFail($id);

        // Parse data from the $post variable
        $this->post_id = $id;
        $this->name = $post->name;
        $this->guard_name = $post->guard_name;

        // Then input fields and show data
        $this->openModal();
    }

    // Delete data
    public function delete($id)
    {
        // Find existing photo
        $sql = Role::select('id')->where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Then delete it

        // Show an alert
        $this->alert('warning', 'Data Has Been Deleted Successfully!');
    }

    public function fileExport() 
    {
        return Excel::download(new RoleExport, 'role-list.xlsx');
    } 

}
