<?php

namespace App\Http\Livewire;

use App\Exports\UsersExport;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithPagination;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role as ModelsRole;

class UserLivewire extends Component
{
    // Load addon trait
    use WithPagination, LivewireAlert;

    // Bootsrap pagination
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpen = 0;
    public $paginatedPerPages = 10;
    public $post_id, $searchTerm, $name, $email, $phone, $role, $password, $password_confirmation;

    // View extending the App Layouts, so we won't do this on Blade
    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.user.user', [
            'lists' =>
            User::
            select(
                'users.id',
                'users.name',
                'users.email',
                'users.phone',
                'roles.name as roles',
                'users.created_at',
                'users.updated_at',
            )->where([
                ['users.name', 'like', '%' . $searchData . '%'],
            ])
            ->join('model_has_roles', ['model_has_roles.model_id' => 'users.id'])
            ->join('roles', ['roles.id' => 'model_has_roles.role_id'])
            ->paginate($this->paginatedPerPages),

            // Base
            'bases' => Role::get(),

        ])->extends('layouts.app');
    }

    // Reset input fields
    private function resetInputFields()
    {
        $this->reset([
            'post_id', 'name', 'email', 'phone', 'password', 'password_confirmation', 'role'
        ]);
    }

    // Open input form
    public function openModal()
    {
        $this->isOpen = true;
    }

    // Close input form
    public function closeModal()
    {
        $this->isOpen = false;
    }

    // Open input form and then reset input fields
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
    }

    public function store()
    {
        // Send a custom message if something is error
        $messages = [
            '*.required'                => 'This column is required',
            '*.numeric'                 => 'This column is required to be filled in with number',
            '*.string'                  => 'This column is required to be filled in with letters',
        ];

        

        // Photo Name with Regex - Replace anything weird with underscore


        // Upload Photo if this is a 'Create'
        if ($this->post_id == false) {
            // Validate input with custom message
            $this->validate([
                'name' => ['required'],
                'phone' => ['required'],
                'password' => ['required','confirmed'],
                'password_confirmation' => ['required'],
                'role' => ['required'],
                'email' => ['required','email','unique:users,email,'.$this->post_id],
            ], $messages); // Delete this '$messages' variable if you don't want to use the custom message validator
            
            $new_user = User::Create([
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'password' => Hash::make($this->password),
            ]);

            $role = ModelsRole::find($this->role);
            if($role)
            {
                $new_user->assignRole($role);
            }
        }

        // Delete Existing Photo and then Upload the New One if this is an 'Update'
        elseif ($this->post_id == true) {
            // Validate input with custom message
            $this->validate([
                'name' => ['required'],
                'phone' => ['required'],
                'password' => ['confirmed'],
                'role' => ['required'],
                'email' => ['required','email','unique:users,email,'.$this->post_id],
            ], $messages); // Delete this '$messages' variable if you don't want to use the custom message validator
            
            $update_user = User::findOrFail($this->post_id);

            $update_user->update([
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
            ]);

            if($this->password)
            {
                $update_user->update(['password' => Hash::make($this->password)]);
            }

            
             $role = ModelsRole::find($this->role);
             if($role)
             {
                 $update_user->syncRoles([$role]);
             }
        }

        // Insert or Update if Ok
       

        // Show an alert
        $this->alert('success', $this->post_id ? 'Data Has Been Updated Successfully!' : 'Data Has Been Submited Successfully!');

        // Close input form, we're going back to the list
        $this->closeModal();

        // Reset input fields for next input
        $this->resetInputFields();
    }

    public function edit($id)
    {
        // Find data from the $id
        $post = User::findOrFail($id);

        // Parse data from the $post variable
        $this->post_id = $id;
        $this->name = $post->name;
        $this->email = $post->email;
        $this->phone = $post->phone;

        $role = DB::select('select role_id from model_has_roles where model_id = ? ', [$post->id]);
        $this->role = ($role != null ? $role[0]->role_id : null);
        

        // Then input fields and show data
        $this->openModal();
    }

    // Delete data
    public function delete($id)
    {
        // Find existing photo
        $sql = User::select('id')->where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Then delete it

        // Show an alert
        $this->alert('warning', 'Data Has Been Deleted Successfully!');
    }

    public function fileExport() 
    {
        return Excel::download(new UsersExport, 'user-list.xlsx');
    } 

}
