<?php

namespace App\Http\Livewire\Home;

use App\Models\EventParticipants;
use App\Models\Events;
use App\Models\OfflineExpo;
use App\Models\OfflineExpoParticipant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use PHPQRCode\QRcode;

class HomeLivewire extends Component{
    // Count start from zero
    public $count = 0;
    
    // Increment function
    public function increment(){
        $this->count++;
    }

    // Decrement function
    public function decrement(){
        $this->count--;
    }

    // View extending the App Layouts, so we won't do this on Blade
    public function render(){
        return view('livewire.home.home-livewire')->extends('layouts.app');
    }

    public function goToExpo(){
        $userData = Auth::user()->toArray();
        $userId = $userData['id'];

        return view('livewire.home.expo-view-v1', compact('userId','userData'));
    }

    public function yourqr(){
        $userData = Auth::user()->toArray();
        $userId = $userData['id'];
        $qr_path = $userData['qr_path'];

        $fileExist = ($qr_path == null ? 0 : (file_exists(storage_path().'/app/public/'.$qr_path) ? 1 : 0));        

        return view('livewire.home.barcode-user', compact('userId','fileExist','qr_path'));
    }

    public function generateNewQr(){
        $userData = Auth::user()->toArray();
        $userId = $userData['id'];

        $path = storage_path() . '/app/public/qr';
        $fileQr = $path . '/' . $userId . '.png';

        User::where([
            'id' => $userId
        ])->update([
            'qr_path' => 'qr/'. $userId . '.png'
        ]);

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true);
        }

        QRcode::png((string) $userId,$fileQr,'H',10,10);
        chmod($fileQr,0777);

        return redirect('/home/your-qr');
    }


    public function validVisitor(Request $request){
        //var_dump($request->id_rsvp);die;
        
        $explode = explode("|",$request->id_rsvp);

        // var_dump($explode);

        $welcomeText = '';

        if(count($explode) < 2 || count($explode) > 2){
            $success = 0;
            $name = "";
        }else{
            if($explode[1] == "" || $explode[1] == null){
                $success = 0;
                $name = "";
            }else{
                $checkEvent = Events::where([
                    'event_id' => $explode[0]
                ]);

                $checkUser = User::where([
                    'id' => $explode[1]
                ]);

                if($checkEvent->count() < 1 || $checkUser->count() < 1){
                    $success = 0;
                    $name = "";
                }else{
                    $checkValidEvent = EventParticipants::where([
                        'event_id' => $explode[0],
                        'user_id' => $explode[1],
                    ]);

                    if($checkValidEvent->count() < 1){
                        $success = 0;
                        $name = "";
                    }else{

                        $success = 1;
                        $name = $checkUser->get()->toArray()[0]['name'];
                        $welcomeText = 'Welcome to Offline Event - '.$checkEvent->get()->toArray()[0]['event_name'];

                        $checkValidEvent->update([
                            'is_present' => 1
                        ]);
                    }
                }
            }
        }

        // die;
        

        // $getUser = User::find($request->id_rsvp);

        // if(!$getUser){
        //     $success = 0;
        //     $name = '';
        // }else{
        //     $success = 1;
        //     $name = $getUser->name;
        // }

        return view('livewire.home.valid-visitor', compact('name','success','welcomeText'));
    }

    public function validVisitorExpo(Request $request){
        //var_dump($request->id_rsvp);die;
        
        $explode = explode("|",$request->id_rsvp);

        // var_dump($explode);

        $welcomeText = '';

        if(count($explode) < 2 || count($explode) > 2){
            $success = 0;
            $name = "";
        }else{
            if($explode[1] == "" || $explode[1] == null){
                $success = 0;
                $name = "";
            }else{
                $checkExpo = OfflineExpo::where([
                    'off_expo_id' => $explode[0]
                ]);

                $checkUser = User::where([
                    'id' => $explode[1]
                ]);

                if($checkExpo->count() < 1 || $checkUser->count() < 1){
                    $success = 0;
                    $name = "";
                }else{
                    $checkValidEvent = OfflineExpoParticipant::where([
                        'off_expo_id' => $explode[0],
                        'user_id' => $explode[1],
                    ]);

                    if($checkValidEvent->count() < 1){
                        $success = 0;
                        $name = "";
                    }else{

                        $success = 1;
                        $name = $checkUser->get()->toArray()[0]['name'];
                        $welcomeText = 'Welcome to Offline Expo - '.$checkExpo->get()->toArray()[0]['off_expo_name'];

                        $checkValidEvent->update([
                            'is_present' => 1
                        ]);
                    }

                }
            }
        }

        // die;
        

        // $getUser = User::find($request->id_rsvp);

        // if(!$getUser){
        //     $success = 0;
        //     $name = '';
        // }else{
        //     $success = 1;
        //     $name = $getUser->name;
        // }

        return view('livewire.home.valid-visitor', compact('name','success','welcomeText'));
    }

}
