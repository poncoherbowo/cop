<?php

namespace App\Http\Livewire\Event;

use App\Exports\ExpoExport;
use App\Exports\ExpoParticipantsExport;
use App\Models\OfflineExpo;
use App\Models\OfflineExpoParticipant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use PHPQRCode\QRcode;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Mail;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use Maatwebsite\Excel\Facades\Excel;

class OffExpoLivewire extends Component
{
    // Load addon trait
    use WithPagination, LivewireAlert;

    // Bootsrap pagination
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpen = 0;
    public $paginatedPerPages = 10;
    public $post_id, $searchTerm, $off_expo_name, $url, $held_on, $is_active, $is_online;

    // View extending the App Layouts, so we won't do this on Blade
    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.offlineexpo.offlineexpo', [
            'lists' =>
            OfflineExpo::where([
                ['off_expo_name', 'like', '%' . $searchData . '%'],
                ['is_active', '=', 1],
            ])->paginate($this->paginatedPerPages),

            'actives' => [
                [
                    'id' => 1,
                    'name' => 'Yes',
                ],
                [
                    'id' => 0,
                    'name' => 'No',
                ],
                
            ],
        ])->extends('layouts.app');
    }

    // Reset input fields
    private function resetInputFields()
    {
        $this->reset([
            'post_id', 'off_expo_name', 'held_on', 'is_active'
        ]);
    }

    // Open input form
    public function openModal()
    {
        $this->isOpen = true;
    }

    // Close input form
    public function closeModal()
    {
        $this->isOpen = false;
    }

    // Open input form and then reset input fields
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
    }

    public function store()
    {
        // Send a custom message if something is error
        $messages = [
            '*.required'                => 'This column is required',
            '*.numeric'                 => 'This column is required to be filled in with number',
            '*.string'                  => 'This column is required to be filled in with letters',
        ];

        // Validate input with custom message
        $this->validate([
            'off_expo_name' => ['required'],
            'held_on' => ['required'],
            'is_active' => ['required'],
        ], $messages); // Delete this '$messages' variable if you don't want to use the custom message validator

        // Photo Name with Regex - Replace anything weird with underscore


        // Upload Photo if this is a 'Create'
        if ($this->post_id == false) {
        }

        // Delete Existing Photo and then Upload the New One if this is an 'Update'
        elseif ($this->post_id == true) {
        }

        // Insert or Update if Ok
        OfflineExpo::updateOrCreate(['off_expo_id' => $this->post_id], [
            'off_expo_name' => $this->off_expo_name,
            'held_on' => $this->held_on,
            'is_active' => $this->is_active,
        ]);

        // Show an alert
        $this->alert('success', $this->post_id ? 'Data Has Been Updated Successfully!' : 'Data Has Been Submited Successfully!');

        // Close input form, we're going back to the list
        $this->closeModal();

        // Reset input fields for next input
        $this->resetInputFields();
    }

    public function edit($id)
    {
        // Find data from the $id
        $post = OfflineExpo::findOrFail($id);

        // Parse data from the $post variable
        $this->post_id = $id;
        $this->off_expo_name = $post->off_expo_name;
        $this->held_on = $post->held_on;
        $this->is_active = $post->is_active;

        // Then input fields and show data
        $this->openModal();
    }

    public static function isRegist($off_expo_id = null, $userId = null)
    {
        $count = OfflineExpoParticipant::where([
            'off_expo_id' => $off_expo_id,
            'user_id' => $userId,
        ]);

        return $count->count();
    }

    // Delete data
    public function delete($id)
    {
        // Find existing photo
        $sql = OfflineExpo::select('off_expo_id')->where('off_expo_id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Then delete it

        // Show an alert
        $this->alert('warning', 'Data Has Been Deleted Successfully!');
    }

    public function regist($id)
    {
        $path = storage_path() . '/app/public/qr/offlineexpo';
        $userId = Auth::id();
        $userMail = Auth::user()->email;
        $userPhone = Auth::user()->phone;
        $userName = Auth::user()->name;

        $offexpo = OfflineExpo::where('off_expo_id', $id)->first();

        $offexpos = OfflineExpoParticipant::where('off_expo_id', $id)->get();
        if ($offexpos->count() >= 100) {
            $this->alert('error', "Quota Exceed");
        } else {
            $fileQr = $path . '/' . (string) $id . '|' . $userId . '.png';
            $participant = new OfflineExpoParticipant;

            $participant->off_expo_id = $id;
            $participant->user_id = $userId;
            $participant->qr_path = 'qr/offlineexpo/' . (string) $id . '|' . $userId . '.png';
            $participant->save();

            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true);
            }
            // QrCode::size(100)->generate($data->id, $fileQr);

            $qrText = (string) $id . '|' . $userId;
            QRcode::png($qrText, $fileQr, 'H', 10, 10);
            chmod($fileQr, 0777);

            Mail::send('mailexpo', [
                'id' => $userId,
                'name' => $userName,
                'phone' => $userPhone,
                'off_expo_name' => $offexpo->off_expo_name,
            ], function ($message) use ($userMail, $userName, $fileQr) {
                $message->to($userMail, $userName)->subject("Registration COP 2021 Offline Events");
                $message->from('no-reply@cop26virtualexpo2021.com', 'COP 2021');
                $message->attach($fileQr);
            });

            $this->alert('success', "Registered Successfully");
            // return redirect()->to('/event/events');
        }
    }

    public function fileExport() 
    {
        return Excel::download(new ExpoExport, 'expo-list.xlsx');
    } 

    public function fileExportParticipant() 
    {
        return Excel::download(new ExpoParticipantsExport, 'expo-participant-list.xlsx');
    }  
}
