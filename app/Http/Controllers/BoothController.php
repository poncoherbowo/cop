<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use Storage;

use App\Models\User;
use App\Models\Booth;
use App\Models\BoothVisitor as Visitor;
use App\Models\BoothProduct as Product;
// use Modules\Booth\Models\Booth;
// use Modules\Booth\Models\BoothVisitor as Visitor;
// use Modules\Booth\Models\BoothProduct as Product;

class BoothController extends Controller
{

    public function showVisit(Request $request, $slug)
    {

        if (!auth()->check()) return redirect()->route('unauthorized');

        // Check User
        $user = auth()->user();

        // Check Booth
        $booth = Booth::where('slug', $slug)->first();
        if (!$booth) return abort(404);

        // Check Visit
        $visits = Visitor::where('user_id', $user->id)->where('booth_id', $booth->id)->whereDate('created_at', Carbon::today())->get();
        if (count($visits) > 0) return redirect()->route('booth.visit.done');

        $data = [
            'user_id' => $user->id,
            'booth_id' => $booth->id,
        ];
        Visitor::create($data);
        Booth::find($booth->id)->increment('visitor_count');

        return view('booth.visit', [
            'booth' => $booth,
            'user' => $user,
        ]);
    }

    public function visit(Request $request, $slug)
    {

        // Check User
        $user = auth()->user();

        // Check Booth
        $booth = Booth::where('slug', $slug)->first();
        if (!$booth) return abort(404);

        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
        ];

        $visitor = Visitor::where('user_id', $user->id)->where('booth_id', $booth->id)->whereDate('created_at', Carbon::today())->first();
        if ($visitor) {
            Visitor::find($visitor->id)->update($data);
        } else {
            $data['user_id'] = $user->id;
            $data['booth_id'] = $booth->id;
            Visitor::create($data);
            Booth::find($booth->id)->increment('visitor_count');
        }

        return successDataResponse([
            'redirect' => route('booth.visit.done'),
        ]);
    }

    public function doneVisit(Request $request)
    {
        return view('booth.visit-done');
    }

    private $additional = [
        "tokopedia" => [
            "url" => [
                "https://tokopedia.link/VLo5xR99Agb",
                "https://tokopedia.link/GznglNdaBgb",
                "https://tokopedia.link/hmcIh9eaBgb",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
                "https://www.tokopedia.com/discovery/bangga-buatan-indonesia",
            ],
        ],
    ];

    public function product(Request $request, $slug)
    {

        // Check Booth
        $booth = Booth::where('slug', $slug)->first();
        if (!$booth) return abort(404);

        $data_products = [];
        $db_products = Product::where('booth_id', $booth->id)->get();
        foreach ($db_products as $data_product) {
            $data_products[$data_product->product] = $data_product->url;
        }

        $products = [];
        $product_paths = Storage::disk('participant')->files($booth->slug . '/products');
        foreach ($product_paths as $index => $path) {
            $info = pathinfo($path);
            if ($info['basename'] != ".DS_Store") {
                $product['src'] = Storage::disk('participant')->url($booth->slug . "/products/" . $info['basename']);
                $product['url'] = isset($data_products[$info['basename']]) ? $data_products[$info['basename']] : $booth->url;

                $products[] = $product;
            }
        }

        return view('booth.product', [
            'booth' => $booth,
            'products' => $products,
        ]);
    }

    public function addCart()
    {
    }

    public function buy()
    {
    }
}
