@props(['id', 'error'])

<input {{ $attributes }} type="text" class="form-control datetimepicker-input @error($error) is-invalid @enderror" id="{{ $id }}" data-toggle="datetimepicker" data-target="#{{ $id }}"
onchange="this.dispatchEvent(new InputEvent('input'))" data-date-format="YYYY-MM-DD HH:mm" 
/>

{{-- <input {{ $attributes }} type="text" class="form-control datetimepicker-input @error($error) is-invalid @enderror" id="{{ $id }}" data-toggle="datetimepicker" data-target="#{{ $id }}"
onchange="this.dispatchEvent(new InputEvent('input'))" data-date-format="YYYY-MM-DD HH:mm" onclick="document.getElementById(`loadheld_on`).innerHTML = 'visible';"
/> --}}


@push('before-livewire-scripts')
<script type="text/javascript">
    $('#{{ $id }}').datetimepicker({
    	format: 'LT',
    });
</script>
@endpush
