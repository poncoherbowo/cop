@extends('layouts.auth')
@section('content')
    @if (!$is_online)
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div style="text-align: center;">
                        <h3>Thanks for your offline event registration, below is your qr code to enter the event</h3>
                        <h5>Details:</h5>
                        <p>Name : {{ $name }}</p>
                        <p>Phone Number : {{ $phone }}</p>
                        <p>Event : {{ $event_name }}</p>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div style="text-align: center;">
                            <h3>Thanks for your online event registration, below is your qr code to enter the event</h3>
                            <h5>Details:</h5>
                            <p>Name : {{ $name }}</p>
                            <p>Phone Number : {{ $phone }}</p>
                            <p>Event : {{ $event_name }}</p>
                            <p>Zoom Meeting URL : {{ $url }}</p>
                        </div>
                    </div>
                </div>
    @endif

    </div>

@endsection
