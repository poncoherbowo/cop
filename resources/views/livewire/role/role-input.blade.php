<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form method="POST" wire:submit.prevent="store()">
        <div class="card-body">
            <div class="form-row">
                
              
             
                <!-- String -->
                <div class="form-group col-12">
                    <label for="name">Role Name</label>
                    <input type="text" wire:model.defer="name" id="name" class="form-control @error('name') is-invalid @enderror">
                    @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                 <!-- String -->
                 <div class="form-group col-12">
                    <label for="guard_name">Guard Name</label>
                    <input type="text" wire:model.defer="guard_name" id="guard_name" class="form-control @error('guard_name') is-invalid @enderror">
                    @error('guard_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                

            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="store()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>