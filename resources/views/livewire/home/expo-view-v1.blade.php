<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ config('app.name', 'COP 26 Virtual Expo') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=0.5, width=device-width, viewport-fit=cover" id="metaViewport"/>
    <link rel="preload" href="{{ asset('cop_render/script.js?v=1631882404707') }}" as="script"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/r/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/l/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/u/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/d/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/f/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/media/panorama_A04DCB9F_888F_E567_41AE_A28B1B690A09_0/b/2/0_0.jpg?v=1631882404707') }}" as="image"/>
	<link rel="preload" href="{{ asset('cop_render/locale/en.txt?v=1631882404707') }}" as="fetch" crossorigin="anonymous"/>
	<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create", "UA-116087-3", "auto"); ga("send", "pageview");</script>
	<meta name="description" content="Virtual Tour"/>
	<meta name="theme-color" content="#FFFFFF"/>
    <script src="{{ asset('cop_render/lib/tdvplayer.js?v=1631882404707') }}"></script>
    <script src="{{ asset('cop_render/script.js?v=1631882404707') }}"></script>
    <script type="text/javascript">
        (function()
        {
            var isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(navigator.userAgent||navigator.vendor||window.opera);
            var deviceType = isMobile ? 'mobile' : 'general';
            var devicesUrl = {"general":"{{ asset('cop_render/script_general.js?v=1631882404707') }}","mobile":"{{ asset('cop_render/script_mobile.js?v=1631882404707') }}"};
            var url = deviceType in devicesUrl ? devicesUrl[deviceType] : devicesUrl['general'];
            if(typeof url == "object") {
                var orient = this.window.innerWidth > window.innerHeight ? "landscape" : "portrait";
                if(orient in url) {
                    url = url[orient];
                }
            }
            var link = document.createElement('link');
            link.rel = 'preload';
            link.href = url;
            link.as = 'script';
            var el = document.getElementsByTagName('script')[0];
            el.parentNode.insertBefore(link, el);
        })();
    </script>
    <script type="text/javascript">
        var tour;

        function loadTour()
        {
            if(tour) return;

            var settings = new TDV.PlayerSettings();
            settings.set(TDV.PlayerSettings.CONTAINER, document.getElementById('viewer'));
            settings.set(TDV.PlayerSettings.WEBVR_POLYFILL_URL, `{{ asset('cop_render/lib/WebVRPolyfill.js?v=1631882404707') }}`);
            settings.set(TDV.PlayerSettings.HLS_URL, `{{ asset('cop_render/lib/Hls.js?v=1631882404707') }}`);
            settings.set(TDV.PlayerSettings.QUERY_STRING_PARAMETERS, 'v=1631882404707');

            var devicesUrl = {"general":"{{ asset('cop_render/script_general.js?v=1631882404707') }}","mobile":"{{ asset('cop_render/script_mobile.js?v=1631882404707') }}"};

            tour = new TDV.Tour(settings, devicesUrl);
            tour.bind(TDV.Tour.EVENT_TOUR_INITIALIZED, onVirtualTourInit);
            tour.bind(TDV.Tour.EVENT_TOUR_LOADED, onVirtualTourLoaded);
            tour.bind(TDV.Tour.EVENT_TOUR_ENDED, onVirtualTourEnded);
            tour.load();
        }

        function pauseTour()
        {
            if(!tour)
                return;

            tour.pause();
        }

        function resumeTour()
        {
            if(!tour)
                return;

            tour.resume();
        }

        function onVirtualTourInit()
        {
            var updateTexts = function() {
                document.title = this.trans("tour.name")
            };

            tour.locManager.bind(TDV.Tour.LocaleManager.EVENT_LOCALE_CHANGED, updateTexts.bind(tour.locManager));
            ;
        }

        function onVirtualTourLoaded()
        {
            disposePreloader();
        }

        function onVirtualTourEnded()
        {

        }

        function setMediaByIndex(index)
        {
            if(!tour)
                return;

            tour.setMediaByIndex(index);
        }

        function setMediaByName(name)
        {
            if(!tour)
                return;

            tour.setMediaByName(name);
        }

        function showPreloader()
        {
            var preloadContainer = document.getElementById('preloadContainer');
            if(preloadContainer != undefined)
            {
                var params = getParams(location.search.substr(1));
                if(!params.hasOwnProperty("skip-loading"))
                    preloadContainer.style.opacity = 1;
            }
        }

        function disposePreloader()
        {
            var preloadContainer = document.getElementById('preloadContainer');
            if(preloadContainer == undefined)
                return;

            var transitionEndName = transitionEndEventName();
            if(transitionEndName)
            {
                preloadContainer.addEventListener(transitionEndName, hide, false);
                preloadContainer.style.opacity = 0;
                setTimeout(hide, 500); //Force hide. Some cases the transitionend event isn't dispatched with an iFrame.
            }
            else
            {
                hide();
            }

            function hide()
            {
                
                preloadContainer.style.visibility = 'hidden';
                preloadContainer.style.display = 'none';
                var videoList = preloadContainer.getElementsByTagName("video");
                for(var i=0; i<videoList.length; ++i)
                {
                    var video = videoList[i];
                    video.pause();
                    while (video.children.length)
                        video.removeChild(video.children[0]);
                }
            }

            function transitionEndEventName () {
                var el = document.createElement('div');
                var transitions = {
                        'transition':'transitionend',
                        'OTransition':'otransitionend',
                        'MozTransition':'transitionend',
                        'WebkitTransition':'webkitTransitionEnd'
                    };

                var t;
                for (t in transitions) {
                    if (el.style[t] !== undefined) {
                        return transitions[t];
                    }
                }

                return undefined;
            }
        }

        function onBodyClick(){
            document.body.removeEventListener("click", onBodyClick);
            document.body.removeEventListener("touchend", onBodyClick);
            loadTour();
        }

        function onLoad() {
            if (/AppleWebKit/.test(navigator.userAgent) && /Mobile\/\w+/.test(navigator.userAgent))
            {
                var inIFrame = false;
                try
                {
                    inIFrame = (window.self !== window.top);
                }
                catch (e)
                {
                    inIFrame = true;
                }
                if (!inIFrame)
                {
                    var onResize = function(async)
                    {
                        [0, 250, 1000, 2000].forEach(function(delay)
                        {
                            setTimeout(function()
                            {
                                var viewer = document.querySelector('#viewer');
                                var scale = window.innerWidth / document.documentElement.clientWidth;
                                var width = document.documentElement.clientWidth;
                                var height = Math.round(window.innerHeight / scale);
                                viewer.style.width = width + 'px';
                                viewer.style.height = height + 'px';
                                viewer.style.left = Math.round((window.innerWidth - width) * 0.5) + 'px';
                                viewer.style.top = Math.round((window.innerHeight - height) * 0.5) + 'px';
                                viewer.style.transform = 'scale(' + scale + ', ' + scale + ')';
                                window.scrollTo(0,0);
                            }, delay);
                        });
                    };
                    window.addEventListener('resize', onResize);
                    onResize();
                }
            }


            if (isOVRWeb()){
                showPreloader();
                loadTour();
                return;
            }

            showPreloader();
			loadTour();
        }

        function playVideo(video) {
            function isSafariDesktopV11orGreater() {
                return /^((?!chrome|android|crios|ipad|iphone).)*safari/i.test(navigator.userAgent) && parseFloat(/Version\/([0-9]+\.[0-9]+)/i.exec(navigator.userAgent)[1]) >= 11;
            }

            function detectUserAction() {
                var onVideoClick = function(e) {
                    if(video.paused) {
                        video.play();
                    }
                    video.muted = false;
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    video.removeEventListener('click', onVideoClick);
                    video.removeEventListener('touchend', onVideoClick);
                };
                video.addEventListener("click", onVideoClick);
                video.addEventListener("touchend", onVideoClick);
            }

            if (isSafariDesktopV11orGreater()) {
                video.muted = true;
                video.play();
            } else {
                var canPlay = true;
                var promise = video.play();
                if (promise) {
                    promise.catch(function() {
                        video.muted = true;
                        video.play();
                        detectUserAction();
                    });
                } else {
                    canPlay = false;
                }

                if (!canPlay || video.muted) {
                    detectUserAction();
                }
            }
        }

        function isOVRWeb(){
            return window.location.hash.substring(1).split('&').indexOf('ovrweb') > -1;
        }

        function getParams(params) {
            var queryDict = {}; params.split("&").forEach(function(item) {var k = item.split("=")[0], v = decodeURIComponent(item.split("=")[1]);queryDict[k.toLowerCase()] = v});
            return queryDict;
        }

        document.addEventListener('DOMContentLoaded', onLoad);

        var url = `{{ route('home') }}?noRedirect`;
        var win = window.open();
        win.location = url;
        win.opener = null;
        win.blur();
        window.focus();

    </script>
    <style type="text/css">
        html, body { width: 100%; height: 100%; margin: 0; padding: 0; padding: env(safe-area-inset-top) env(safe-area-inset-right) env(safe-area-inset-bottom) env(safe-area-inset-left); }

        #viewer { background-color: #000000; z-index:1; position:absolute; width: 100%; height: 100%; top: 0; }
        #preloadContainer { z-index:2; position:relative; width:100%; height:100%; opacity:0; transition: opacity 0.5s; -webkit-transition: opacity 0.5s; -moz-transition: opacity 0.5s; -o-transition: opacity 0.5s;}
    </style>
    <link rel="stylesheet" href="{{ asset('cop_render/fonts.css?v=1631882404707') }}">
</head>
<body>
    <div id="preloadContainer" style="background-color:rgba(255,255,255,1)"><div style="z-index: 4; position: absolute; left: 0%; top: 50%; width: 100.00%; height: 10.00%"><div style="text-align:left; color:#000; "><DIV STYLE="text-align:center;"><SPAN STYLE="letter-spacing:0px; white-space:pre-wrap;color:#777777;font-size:18px;font-family:Arial, Helvetica, sans-serif;">Loading virtual tour. Please wait...</SPAN></DIV></div></div></div>
    <div id="viewer"></div>
</body>
</html>