@if (Auth::user()->hasRole([1]))
    {{-- @include('livewire.home.scan-html5') --}}
    @include('livewire.home.scan-bootstrap')
@else
    @include('livewire.home.user-view')
@endif

@if (!isset($_GET['noRedirect']) && !isset($_GET['scanType']))
<script>
    
    var url = `{{ route('goToExpo') }}`;

    window.location.href = url;
</script>
@endif