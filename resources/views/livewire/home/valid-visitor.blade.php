@extends('layouts.app')
@section('title', 'Validation Visitor')
@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{ route('home') }}">
            <i class="fa fa-qrcode"></i> Back To Scan
        </a>
    </div>
    <div class="card-body">
        @if ($success)
            <p class="m-0">{{ $welcomeText }}</p>
            <div class="border text-center my-2 p-3">
                <h1 class="my-0">{{ $name }}</h1>
            </div>
            <p class="m-0">Thanks for your participate!</p>
        @else
            <div class="border text-center my-2 p-3">
                <strong style="color:red">QR Is Not Valid.</strong>
            </div>
        @endif
    </div>
</div>
@endsection