@extends('layouts.app')
@section('title', 'Your QR')
@section('content')
<div class="card">
    <div class="card-header">
        <strong>Your QR</strong>
    </div>
    <div class="card-body">
        <div class="border text-center my-2 p-3">
            @if($fileExist == 1)
                <img src="{{ asset('storage/'.$qr_path)  }}" alt="" class="d-md-none d-lg-none d-xl-none" width="100%" height="100%">
                <img src="{{ asset('storage/'.$qr_path)  }}" alt="" class="d-none d-sm-inline" >
                <a href="{{ asset('storage/'.$qr_path)  }}" download="" class="form-control btn btn-primary">Download This QR</a>
            @else
                <a href="{{ route('generateNewQr') }}" class="form-control btn btn-primary">Generate Now!</a>
            @endif
        </div>
    </div>
</div>
@endsection