<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form method="POST" wire:submit.prevent="store()">
        <div class="card-body">
            <div class="form-row">
                
              
             
                <!-- String -->
                <div class="form-group col-12">
                    <label for="name">Name</label>
                    <input type="text" wire:model.defer="name" id="name" class="form-control @error('name') is-invalid @enderror">
                    @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                 <!-- String -->
                 <div class="form-group col-12">
                    <label for="email">Email</label>
                    <input type="email" wire:model.defer="email" id="email" class="form-control @error('email') is-invalid @enderror">
                    @error('email') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                <div class="form-group col-12">
                    <label for="phone">Phone</label>
                    <input type="number" wire:model.defer="phone" id="phone" class="form-control @error('phone') is-invalid @enderror">
                    @error('phone') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                <div class="form-group col-12">
                    <label for="role">Role</label>
                    <select wire:model="role" class="form-control @error('role') is-invalid @enderror" required="required">
                        <option value="" selected="selected">- Select -</option>
                        @foreach($bases AS $base)
                        <option value="{{ $base->id }}">{{ $base->name }}</option>
                        @endforeach
                    </select>
                    @error('role') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                <div class="form-group col-12">
                    <label for="password">Password</label>
                    <input type="password" wire:model.defer="password" id="password" class="form-control @error('password') is-invalid @enderror">
                    @error('password') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>

                <div class="form-group col-12">
                    <label for="password_confirmation">Password Confirmation</label>
                    <input type="password" wire:model.defer="password_confirmation" id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
                    @error('password_confirmation') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                
                

            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="store()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>