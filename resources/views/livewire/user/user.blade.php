@section('title', 'User')


@if ($isOpen)
    @include('livewire.user.user-input')
@else
<div class="card">
    <div class="card-header">
        @if (Auth::user()->hasRole([1]))
        <div class="row">
            <div class="col-12">
                <button style="margin-bottom: 5px;" wire:click="create()" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</button>
            
                &nbsp;<br>
            
                <button style="margin-bottom: 5px;" wire:click="fileExport()" class="btn btn-dark"><i class="fas fa-file pr-1"></i> Export Excel</button>
                
            </div>
        </div>
    <br>
    @endif
        <div class="row">
            <div class="col-12">
                <input type="text" wire:model="searchTerm" placeholder="Search Event Name Here .." class="form-control">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead class="text-center">
                    <tr>
                        <th width="5%">No</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Email</th>
                        <th class="text-left">Phone</th>
                        <th class="text-left">Role</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($lists as $list)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td class="text-left">{{ $list['name'] }}</td>
                            <td class="text-left">{{ $list['email'] }}</td>
                            <td class="text-left">{{ $list['phone'] }}</td>
                            <td class="text-left">{{ $list['roles'] }}</td>
                            <td>
                                
                                    <button wire:click="edit({{ $list['id'] }})" class="btn btn-sm btn-info"><i class="fas fa-edit"></i></button>
                                    <button wire:click="delete({{ $list['id'] }})" class="btn btn-sm btn-danger" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>
                               
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if ($lists->hasPages())
            {{ $lists->links() }}
        @endif
    </div>
</div>
@endif
