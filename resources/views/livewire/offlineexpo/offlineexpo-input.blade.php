<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form method="POST" wire:submit.prevent="store()">
        <div class="card-body">
            <div class="form-row">
                
              
             
                <!-- String -->
                <div class="form-group col-12">
                    <label for="off_expo_name">Expo Name</label>
                    <input type="text" wire:model.defer="off_expo_name" id="off_expo_name" class="form-control @error('off_expo_name') is-invalid @enderror">
                    @error('off_expo_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>


                <!-- Number -->
                <div class="form-group col-12">
                    <label for="held_on">Held On</label>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button type="button" class="input-group-text" ><i class="fas fa-clock"></i></button>
                        </div>
                        <x-datepicker wire:model.defer="held_on" id="held_on" :error="'held_on'"  />
                        <span id="loadheld_on" class="form-control" style="color:red">If The Datepicker Not Appear, Type Any Key Then Click Again</span>
                        @error('held_on') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>

                </div>

                 <!-- Select -->
                 <div class="form-group col-12">
                    <label for="is_active">Is Active?</label>
                    <select wire:model.defer="is_active" class="form-control @error('is_active') is-invalid @enderror" required="required">
                        <option value="" selected="selected">- Select -</option>
                        @foreach($actives AS $active)
                        <option value="{{ $active['id'] }}">{{ $active['name'] }}</option>
                        @endforeach
                    </select>
                    @error('is_active') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                

            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="store()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>