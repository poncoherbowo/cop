@section('title', 'Talkshow Events')
@inject('eventFunc', 'App\Http\Livewire\Event\EventLivewire')

@if ($isOpen)
    @include('livewire.events.event-input')
@else
    <div class="card">
        <div class="card-header">
        @if (Auth::user()->hasRole([1]))
            <div class="row">
                <div class="col-12">
                    <button style="margin-bottom: 5px;" wire:click="create()" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</button>
                
                    &nbsp;<br>
                
                    <button style="margin-bottom: 5px;" wire:click="fileExport()" class="btn btn-dark"><i class="fas fa-file pr-1"></i> Export Excel</button>
                    
                    &nbsp;<br>
                
                    <button style="margin-bottom: 5px;" wire:click="fileExportParticipant()" class="btn btn-dark"><i class="fas fa-file pr-1"></i> Export Participant Excel</button>
                </div>
            </div>
        <br>
        @endif
            <div class="row">
                <div class="col-12">
                    <input type="text" wire:model="searchTerm" placeholder="Search Event Name Here .." class="form-control">
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead class="text-center">
                        <tr>
                            <th width="5%">No</th>
                            <th class="text-left">Event Name</th>
                            <th class="text-left">Is Online ?</th>
                            <th class="text-left">Held On Jakarta</th>
                            <th class="text-left">Held On Glasgow</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($lists as $list)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td class="text-left">{{ $list['event_name'] }}</td>
                                <td class="text-left">{{ ($list['is_online'] == 1 ? 'Yes' : 'No') }}</td>
                                <td class="text-left">{{ date('D d M Y H:i', strtotime($list['held_on']))  }}</td>
                                <td class="text-left">
                                    <?php 
                                        $date = new DateTime($list['held_on'], new DateTimeZone('Asia/Jakarta'));  
                                        $date->setTimezone(new DateTimeZone('Europe/Berlin'))
                                    ?>
                                    
                                    {{ $date->format('D d M Y H:i') }}
                                </td>
                                <td>
                                    @if(Auth::user()->hasRole([1]))
                                        <button wire:click="edit({{ $list['event_id'] }})" class="btn btn-sm btn-info"><i class="fas fa-edit"></i></button>

                                        <button wire:click="delete({{ $list['event_id'] }})" class="btn btn-sm btn-danger" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>
                                    @else
                                        @if ($eventFunc::isRegist($list['event_id'], Auth::user()->id))
                                            You've been registered
                                        @else
                                            <?php
                                                $date_now = time(); 
                                                $date_convert = strtotime($list['held_on']);

                                            ?>
                                            @if ($date_convert > $date_now)
                                                <button wire:click="regist({{ $list['event_id'] }})" class="btn btn-sm btn-info" id="btnRegist-{{ $list['event_id'] }}" onclick="document.getElementById(`btnRegist-{{ $list['event_id'] }}`).style.visibility = 'hidden';document.getElementById(`loadRegist-{{ $list['event_id'] }}`).style.visibility = 'visible';">Register</button>
                                            @endif
                                            
                                            <div wire:loading id="loadRegist-{{ $list['event_id'] }}" style="visibility: hidden">
                                                Processing ...
                                            </div>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @if($lists->hasPages())
                {{ $lists->links() }}
            @endif
        </div>
    </div>
@endif