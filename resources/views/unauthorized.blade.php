@extends('layouts.app_booth')

@section('title', 'Guest Book - ')

@section('content')
    <div class="row h-100 w-100 align-items-center g-0">
        <div class="col">

            <div class="mx-auto p-4 rounded-3" style="width: 90%; max-width: 400px;">

                <h1 class="text-center fw-bold mb-3">Unauthorized.</h1>
                <h5 class="text-center">Silahkan refresh halaman ini, untuk melakakuan login kembali.</h5>

            </div>

        </div>
    </div>
@endsection