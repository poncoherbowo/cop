<!DOCTYPE html>
<html class="w-full h-full" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title'){{ config('app.name') }}</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @stack('css')
    </head>

    <body class="w-full min-h-full p-0 m-0 bg-bamboo bg-norepeat bg-cover bg-top-right">
        <div class="flex flex-col h-full justify-center items-center">
            <div class="w-10/12 md:my-0 md:mx-auto">
                <div class="invisible sm:w-0 sm:h-0 md:visible">
                    <div class="md:flex-1">
                        <img class="block h-28" src="{{ asset('img/logo-klhk.png') }}" alt="" />
                    </div>
                    <div class="md:flex-1">
                        <img class="block h-44" src="{{ asset('img/logo-un-climate-change.png') }}" alt="" />
                    </div>
                </div>
                <div class="visivle sm:invisible md:flex justify-between items-start">
                    <div class="md:flex-1">
                        <img class="block h-28" src="{{ asset('img/logo-klhk.png') }}" alt="" />
                    </div>
                    <div class="md:flex-1"></div>
                    <div class="md:flex-1">
                        <img class="block h-44" src="{{ asset('img/logo-un-climate-change.png') }}" alt="" />
                    </div>
                </div>
            </div>
        </div>
            
        <script src="{{ asset('js/app-web.js') }}"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
        @stack('js')
    </body>

</html>

<style>
    header {
        /* position: relative;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        background-size: cover;
        background-repeat: no-repeat; */
    }

    .mw-50 {
        width: 100%;
        height: auto;
    }

    .body-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-image/LPP.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        /* background-size: 100% 100%; */
        background-position: center center;
        background-attachment: fixed;
    }

    .footer-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-img/Bottom.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: bottom;
        margin-top: 7rem !important;
    }

    .rounded-btn-box {
        border-radius: 40px;
        border: 2px solid white;
        background: white;
    }

    .rounded-box-body {
        padding: 0rem !important;
    }

    .btn-lp {
        border-radius: 40px;
        cursor: pointer;
    }

    .btn-lp:hover {
        background-color: #37328c !important;
        color: white;
    }

    svg {
        position: absolute;
        width: 100%;
        height: auto;
    }

    section {
        width: 100%;
    }

    .curve {
        position: absolute;
        height: 250px;
        width: 100%;
        bottom: 0;
        text-align: center;
    }

    .curve::before {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        transform: translate(85%, 60%);
        background-color: hsl(0, 0%, 100%);
    }

    .curve::after {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        transform: translate(-4%, 40%);
        z-index: -1;
        background-repeat: no-repeat;
        background-position: -1% 97%;
    }

    .modal-dialog {
        max-width: 1000px;
        margin: 30px auto;
    }



    .modal-body {
        position: relative;
        padding: 0px;
    }

    .close {
        position: absolute;
        right: -30px;
        top: 0;
        z-index: 999;
        font-size: 2rem;
        font-weight: normal;
        color: #fff;
        opacity: 1;
    }

    body {
        margin: 2rem;
    }

</style>