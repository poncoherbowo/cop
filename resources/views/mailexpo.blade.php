@extends('layouts.auth')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div style="text-align: center;">
                    <h3>Thanks for your offline Expo registration, below is your qr code to enter the event</h3>
                    <h5>Details:</h5>
                    <p>Name : {{ $name }}</p>
                    <p>Phone Number : {{ $phone }}</p>
                    <p>Expo : {{ $off_expo_name }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
