@extends('layouts.auth')
@section('title', 'Login')
@section('content')
    <div class="login-box">
        {{-- <div class="login-logo">
            <a href="/"><strong>@yield('title')</strong></a>
        </div> --}}
        <div class="card card-login">
            <div class="card-body login-card-body">
                <form action="{{ route('login') }}" method="POST">
                    <div class="input-group mb-3">
                        <input type="email" name="email" value="{{ old('email') }}"
                            class="form-control @error('email') is-invalid @enderror" placeholder="Email"
                            required="required">
                        <div class="input-group-append">
                            <div class="input-group-text inpt-email">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                        <div class="alert alert-danger">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
                    <div class="input-group mb-3" style="display: none;">
                        <input type="password" name="password" value="copv2021"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Password"
                            required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                        <div class="alert alert-danger">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
                    <div class="d-none">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="row">

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block btn-lp">Login</button>
                        </div>

                        <div class="col-12 mt-3">
                            <a class="btn btn-primary btn-block btn-lp" href="{{ route('register') }}">Register</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<style>
    .btn-lp {
        border-radius: 40px;
        cursor: pointer;
        background-color: #37328c !important;
    }

    .card-login {
        background-color: transparent !important;
    }

    /* .login-box {
        margin-top: 600px !important;
    } */

    .login-card-body {
        background-color: transparent !important;
    }

    .inpt-email {
        background-color: rgb(228, 228, 228) !important;
    }

</style>
