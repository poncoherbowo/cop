<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title'){{ config('app.name') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('frontend/css/app.css') }}">
    @stack('css')
</head>

<body class="body-bg">
    {{-- <header> --}}
    <div class="container-fluid ">
        <div class="row">
            <div class="col mt-4 text-right">
                {{-- <img src="{{ asset('img/cop-img/LogoKLHK.png') }}" class="mw-50" /> --}}
            </div>
            <div class="col mt-4">
                <div class="card rounded-btn-box">
                    <div class="card-body rounded-box-body">
                        <div class="col-12 text-center">
                            {{-- <a href="{{ route('login') }}" class="btn mx-3 btn-lp">Virtual Expo</a> --}}
                            <button type="button" class="btn mx-3 btn-lp" data-bs-toggle="modal"
                                data-bs-target="#exampleModalLong">
                                Virtual Expo
                            </button>
                            <a href="{{ route('login') }}" class="btn mx-3 btn-lp">Talk Show</a>
                            <a href="#" class="btn mx-3 btn-lp">Contact</a>
                            <a href="#" class="btn mx-3 btn-lp">About</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mt-3">
                {{-- <img src="{{ asset('img/cop-img/LogoUNKanan.png') }}" class="mw-50" /> --}}
            </div>
        </div>
        <div class="row mt-3">
            <div class="col d-flex justify-content-center">
                {{-- <img src="{{ asset('img/cop-img/LogoCOP26Highres.png') }}" class="img-fluid mw-50" /> --}}
            </div>
        </div>
        <div class="row">
            <div class="col d-flex justify-content-center my-5">
                {{-- <img src="{{ asset('img/cop-img/Judul.png') }}" class="img-fluid mw-50" /> --}}
            </div>
        </div>
        {{-- <div class="row">
                <div class="col d-flex justify-content-center my-5">
                    <div class="curve"></div>
                </div>
            </div> --}}

        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div>
                            <iframe width="100%" height="800"
                                src="{{ asset('img/cop-img/virtual-expo.mp4') }}"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </header> --}}

    {{-- <section class="footer-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col d-flex justify-content-center my-5 ">
                    <img src="{{ asset('img/cop-img/COP_UNFCCC.png') }}" class="img-fluid" />
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-center mt-5" style="margin-bottom: 7rem">
                    <img src="{{ asset('img/cop-img/FotoDokumentasi.png') }}" class="img-fluid"
                        alt="FotoDokumentasi">
                </div>
            </div>
        </div>



    </section> --}}
    <script src="{{ asset('frontend/js/app-web.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
    @stack('js')
</body>

</html>

<style>
    header {
        /* position: relative;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        background-size: cover;
        background-repeat: no-repeat; */
    }

    .mw-50 {
        width: 100%;
        height: auto;
    }

    .body-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-image/LPP.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        /* background-size: 100% 100%; */
        background-position: center center;
        background-attachment: fixed;
    }

    .footer-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-img/Bottom.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: bottom;
        margin-top: 7rem !important;
    }

    .rounded-btn-box {
        border-radius: 40px;
        border: 2px solid white;
        background: white;
    }

    .rounded-box-body {
        padding: 0rem !important;
    }

    .btn-lp {
        border-radius: 40px;
        cursor: pointer;
    }

    .btn-lp:hover {
        background-color: #37328c !important;
        color: white;
    }

    svg {
        position: absolute;
        width: 100%;
        height: auto;
    }

    section {
        width: 100%;
    }

    .curve {
        position: absolute;
        height: 250px;
        width: 100%;
        bottom: 0;
        text-align: center;
    }

    .curve::before {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        transform: translate(85%, 60%);
        background-color: hsl(0, 0%, 100%);
    }

    .curve::after {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        transform: translate(-4%, 40%);
        z-index: -1;
        background-repeat: no-repeat;
        background-position: -1% 97%;
    }

    .modal-dialog {
        max-width: 1000px;
        margin: 30px auto;
    }



    .modal-body {
        position: relative;
        padding: 0px;
    }

    .close {
        position: absolute;
        right: -30px;
        top: 0;
        z-index: 999;
        font-size: 2rem;
        font-weight: normal;
        color: #fff;
        opacity: 1;
    }

    body {
        margin: 2rem;
    }

</style>
