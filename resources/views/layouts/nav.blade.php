<nav class="main-header navbar navbar-expand navbar-light">
    {{-- <ul class="navbar-nav">
        <li class="nav-item">
            <a href="#" class="nav-link" data-widget="pushmenu">
                <i class="fas fa-bars"></i>
            </a>
        </li>
    </ul> --}}
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="{{ route('home') }}" class="{{ request()->routeIs('home') ? 'nav-link active' : 'nav-link' }}">
                {{-- <i class="{{ request()->routeIs('home') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                <p>Home</p>
            </a>
        </li>
        @if (Auth::user()->hasRole([1]))
            {{-- <li class="nav-item"> --}}
            {{-- <a href="{{ route('example.crud') }}" class="{{ request()->routeIs('example.crud') ? 'nav-link active' : 'nav-link' }}"> --}}
            {{-- <i class="{{ request()->routeIs('example.crud') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
            {{-- <p>CRUD Example</p> --}}
            {{-- </a> --}}
            {{-- </li> --}}
        @endif
        <li class="nav-item">
            <a href="{{ route('goToExpo') }}"
                class="{{ request()->routeIs('goToExpo') ? 'nav-link active' : 'nav-link' }}" target="_blank">
                {{-- <i class="{{ request()->routeIs('goToExpo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                <p>Go To Virtual Expo</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('events') }}"
                class="{{ request()->routeIs('events') ? 'nav-link active' : 'nav-link' }}">
                {{-- <i class="{{ request()->routeIs('events') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                <p>Talk Show</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('expo') }}" class="{{ request()->routeIs('expo') ? 'nav-link active' : 'nav-link' }}">
                {{-- <i class="{{ request()->routeIs('expo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                <p>Expo</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
        </li>
    </ul>
</nav>
