<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }}. {{ config('app.name', 'COP26') }}. All Rights Reserved.</strong>
    {{-- <div class="float-right d-none d-sm-inline">
        <small>Built with <i class="fas fa-heart text-pink"></i> <a href="#">COP 26 Virtual Expo</a></small>
    </div> --}}
</footer>