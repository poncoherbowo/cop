<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>@yield('title', 'Fortify') | {{ config('app.name', 'COP 26 Virtual Expo') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Fontawesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet">

    <!-- AdminLTE -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/css/adminlte.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/themes/dark.css"> --}}

    {{-- <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/fontawesome-free/css/all.min.css"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('plugins/fa-all.min.css') }}"> --}}

    {{-- <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app1.css') }}?v={{ config('app.asset_version') }}">
    @stack('css')
    <!-- Livewire -->
    <livewire:styles />
</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">
        {{-- @include('layouts.nav-new') --}}
        {{-- @include('layouts.nav') --}}
        {{-- @include('layouts.aside') --}}
        {{-- <main role="main"> --}}
            <section class="content-wrapper body-bg">
                <div class="content-header">
                    <div class="container-fluid">
                        <h1 class="m-0 text-white">@yield('title')</h1>
                    </div>
                </div>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        {{-- </main> --}}
        {{-- <aside class="control-sidebar control-sidebar-dark d-none">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </aside> --}}
        {{-- @include('layouts.footer') --}}
    </div>
    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    {{-- <script src="https://adminlte.io/themes/v3/plugins/moment/moment.min.js"></script> --}}
    <script src="{{ asset('plugins/moment.min.js') }}"></script>

    <script src="{{ asset('plugins/tempusdominus-bootstrap-4.min.js') }}"></script>
   
    {{-- <script type="text/javascript">

        $('#held_on').datetimepicker({
                format: 'LT',
                icons: { time: 'fas fa-clock' }
        });

    </script> --}}
{{-- 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>

    <script>
       $(document).ready(function(){
            $("#held_on").flatpickr({
                enableTime: true,
                dateFormat: "Y-m-d H:i"
            });
       })
    </script> --}}

    

    <!-- Bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/adminlte.min.js"></script>

    <!-- Livewire -->
    <livewire:scripts />

    <!-- Alert -->
    @livewireAlertScripts

    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <x-livewire-alert::scripts /> --}}

    <!-- Turbolinks -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ asset('js/app1.js') }}?v={{ config('app.asset_version') }}"></script>
    @stack('js')
</body>

</html>


<style>
    .body-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        /* background-size: 100% 100%; */
        background-position: center center;
        background-attachment: fixed;
    }

    .rounded-btn-box {
        border-radius: 40px;
        border: 2px solid white;
        background: white;
    }

    .rounded-box-body {
        padding: 0rem !important;
    }

    .btn-lp {
        border-radius: 40px;
        cursor: pointer;
    }

    .btn-lp:hover {
        background-color: #37328c !important;
        color: white;
    }

</style>
