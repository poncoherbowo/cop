<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Auth') | {{ config('app.name', 'Gampangan') }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/css/adminlte.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
</head>

<body class="hold-transition login-page body-bg">
    {{-- <main role="main"> --}}
        <div class="container-fluid ">
            <div class="row">
                <div class="ml-auto" style="margin-right: 400px; margin-top:100px">
                    @yield('content')
                </div>
            </div>
        </div>
    {{-- </main> --}}
    {{-- <a href="{{ url('/') }}">Back to {{ config('app.name', 'Gampangan') }}</a>
        <ul class="list-inline">
            <li class="list-inline-item">
                <a href="{{ route('login') }}">Login</a>
            </li>
            <li class="list-inline-item">
                <a href="{{ route('register') }}">Register</a>
            </li>
            <li class="list-inline-item">
                <a href="{{ route('password.email') }}">Forgot Password</a>
            </li>
        </ul> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/adminlte.min.js"></script>
</body>

</html>

<style>
    /* header {
        position: relative;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
    } */

    .body-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-image/LPP2.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        /* background-size: 100% 100%; */
        background-position: center center;
        background-attachment: fixed;
    }

    .footer-bg {
        position: relative;
        background-image: url('{{ asset('img/cop-img/Bottom.png') }}');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: bottom;
        margin-top: 7rem !important;
    }

    .rounded-btn-box {
        border-radius: 40px;
        border: 2px solid white;
        background: white;
    }

    .rounded-box-body {
        padding: 0rem !important;
    }

    .btn-lp {
        border-radius: 40px;
        cursor: pointer;
    }

    .btn-lp:hover {
        background-color: #37328c !important;
        color: white;
    }

    svg {
        position: absolute;
        width: 100%;
        height: auto;
    }

    section {
        width: 100%;
    }

    .curve {
        position: absolute;
        height: 250px;
        width: 100%;
        bottom: 0;
        text-align: center;
    }

    .curve::before {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        transform: translate(85%, 60%);
        background-color: hsl(0, 0%, 100%);
    }

    .curve::after {
        content: '';
        display: block;
        position: absolute;
        border-radius: 100% 50%;
        width: 55%;
        height: 100%;
        background-image: url('{{ asset('img/cop-img/BambooBBackground.png') }}');
        transform: translate(-4%, 40%);
        z-index: -1;
        background-repeat: no-repeat;
        background-position: -1% 97%;
    }

</style>
