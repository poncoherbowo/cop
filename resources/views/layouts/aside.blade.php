<aside class="main-sidebar sidebar-dark-primary elevation-4">
    {{-- <a href="{{ url('/') }}" class="brand-link"> --}}
    <span href="{{ url('/') }}" class="brand-link">
        <img src="{{ asset('img/cop-img/LogoCOP26Highres.ico') }}" class="brand-image">
        <span class="brand-text">{{ config('app.name', 'Gampangan') }}</span>
    </span>
    {{-- </a> --}}
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Main Menu -->
                <li class="{{ request()->routeIs(['home', 'validVisitor', 'example.crud', 'your-qr', 'goToExpo', 'events', 'expo']) ? 'nav-item has-treeview menu-open' : 'nav-item' }}">
                    <a href="#" class="{{ request()->routeIs(['home', 'validVisitor', 'example.crud', 'your-qr', 'goToExpo', 'events', 'expo']) ? 'nav-link active' : 'nav-link' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Main Menu <i class="right fas fa-angle-left"></i></p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="{{ request()->routeIs('home') ? 'nav-link active' : 'nav-link' }}">
                                <i class="{{ request()->routeIs('home') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        @if(Auth::user()->hasRole([1]))
                        {{-- <li class="nav-item">
                            <a href="{{ route('example.crud') }}" class="{{ request()->routeIs('example.crud') ? 'nav-link active' : 'nav-link' }}">
                                <i class="{{ request()->routeIs('example.crud') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i>
                                <p>CRUD Example</p>
                            </a>
                        </li> --}}
                        @endif
                        <li class="nav-item">
                            <a href="{{ route('goToExpo') }}" class="{{ request()->routeIs('goToExpo') ? 'nav-link active' : 'nav-link' }}" target="_blank">
                                <i class="{{ request()->routeIs('goToExpo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i>
                                <p>Go To Expo</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('events') }}" class="{{ request()->routeIs('events') ? 'nav-link active' : 'nav-link' }}">
                                <i class="{{ request()->routeIs('events') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i>
                                <p>Talk Show</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('expo') }}" class="{{ request()->routeIs('expo') ? 'nav-link active' : 'nav-link' }}">
                                <i class="{{ request()->routeIs('expo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i>
                                <p>Expo</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>