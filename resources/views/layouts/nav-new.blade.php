<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="{{ route('home') }}" class="navbar-brand">
      {{-- <span  class="navbar-brand"> --}}
        <img src="{{ asset('img/cop-img/LogoCOP26Highres.ico') }}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        {{-- <span class="brand-text font-weight-light">COP 26 Virtual Expo</span> --}}
      {{-- </span> --}}
      </a>

      <button class="navbar-toggler order-1 collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse order-3 collapse" id="navbarCollapse" style="">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{ route('home') }}?noRedirect" class="{{ request()->routeIs('home') ? 'nav-link active' : 'nav-link' }}">
                {{-- <i class="{{ request()->routeIs('home') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                <p>Home</p>
            </a>
          </li>
          @if (Auth::user()->hasRole([1]))
              {{-- <li class="nav-item"> --}}
              {{-- <a href="{{ route('example.crud') }}" class="{{ request()->routeIs('example.crud') ? 'nav-link active' : 'nav-link' }}"> --}}
              {{-- <i class="{{ request()->routeIs('example.crud') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
              {{-- <p>CRUD Example</p> --}}
              {{-- </a> --}}
              {{-- </li> --}}
          @endif
          <li class="nav-item">
              <a href="{{ route('goToExpo') }}"
                  class="{{ request()->routeIs('goToExpo') ? 'nav-link active' : 'nav-link' }}" target="_blank">
                  {{-- <i class="{{ request()->routeIs('goToExpo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                  <p>Go To Virtual Expo</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="{{ route('events') }}"
                  class="{{ request()->routeIs('events') ? 'nav-link active' : 'nav-link' }}">
                  {{-- <i class="{{ request()->routeIs('events') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                  <p>Talk Show</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="{{ route('expo') }}" class="{{ request()->routeIs('expo') ? 'nav-link active' : 'nav-link' }}">
                  {{-- <i class="{{ request()->routeIs('expo') ? 'fas fa-circle nav-icon' : 'far fa-circle nav-icon' }}"></i> --}}
                  <p>Expo</p>
              </a>
          </li>
          

          @if (Auth::user()->hasRole([1]))
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Administration</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="{{ route('role') }}" class="{{ request()->routeIs('role') ? 'nav-link active' : 'nav-link' }} dropdown-item">Roles</a></li>
              <li><a href="{{ route('user') }}" class="{{ request()->routeIs('user') ? 'nav-link active' : 'nav-link' }} dropdown-item">Users</a></li>

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              {{-- <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Report</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">Participant Talkshow</a>
                  </li>
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">Participant Expo</a>
                  </li>

                  <!-- Level three dropdown-->
                  <li class="dropdown-submenu">
                    <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                    <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                    </ul>
                  </li>
                  <!-- End Level three -->

                </ul>
              </li> --}}
              <!-- End Level two -->
            </ul>
          </li>
          @endif
        </ul>

      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
       
        <!-- Notifications Dropdown Menu -->
        {{-- <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li> --}}
        <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-share"></i> Logout
            </a>
        </li>
      </ul>
    </div>
  </nav>