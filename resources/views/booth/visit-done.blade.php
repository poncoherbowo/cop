@extends('layouts.app_booth')

@section('title', 'Guest Book - ')

@section('content')
    <div class="row h-100 w-100 align-items-center g-0">
        <div class="col">

            <div class="mx-auto p-4 rounded-3" style="width: 90%; max-width: 400px;">

                <h3 class="text-center fw-bold mb-3">Terima kasih telah mengisi buku tamu untuk booth ini.</h3>
                <h5 class="text-center">Silahkan tutup <i>popup</i> ini</h5>

            </div>

        </div>
    </div>
@endsection