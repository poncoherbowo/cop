@extends('layouts.app_booth')

@section('title', 'Guest Book - ')

@section('content')
    <div class="row h-100 w-100 align-items-center g-0">
        <div class="col">

            <div class="mx-auto p-4 rounded-3" style="width: 90%; max-width: 400px;">

                <h4 class="text-center pt-5">Buku Tamu</h4>
                <h1 class="text-center fw-bold mb-4">{{ $booth->name }}</h1>

                <form id="form" method="post" action="">
                    <div class="mb-2">
                        <label class="form-label" for="name">Nama Lengkap</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}" required />
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="phone">No. HP / Whatsapp</label>
                        <input type="text" name="phone" id="phone" class="form-control form-phone" value="{{ $user->phone }}" required />
                    </div>
                    <div class="mb-4">
                        <label class="form-label" for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}" required />
                    </div>

                    <button type="submit" id="button" class="btn btn-primary w-100 mb-4">Masuk Booth <i class="ms-2 bi bi-arrow-up-right-square"></i></button>
    
                </form>
            </div>

        </div>
    </div>
@endsection

@push('js')
<script>
    const form = document.getElementById('form');
    form.addEventListener('submit', function(e) {
        e.preventDefault();

        const url = this.getAttribute('action');
        const form_data = new FormData(this);
        const csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        const button = document.getElementById('button');
        const buttonDefaultHTML = button.innerHTML;
        button.setAttribute("disabled", "disabled");
        button.innerHTML = 'Loading..';

        axios({
            method: "post",
            url: url,
            data: form_data,
            headers: {
                "Content-Type": "multipart/form-data",
                "Accept": "application/json",
                "X-CSRF-TOKEN": csrf
            },
        })
        .then((response) => {
            window.location.href = response.data.redirect;
        })
        .catch((error) => {
            const data = error.response.data;

            if (typeof data.code != "undefined") {
                if(data.code == 'input'){
                    alert('Please complete the form correctly.');
                } else if(data.code == 'not-found'){
                    alert('User not found.');
                } else if(data.code == 'wrong-password'){
                    alert('Email or password you entered is not valid.');
                } else {
                    alert('Something went wrong, please try again later.');
                }
            } else {
                alert('Something went wrong, please try again later.');
            }
            
            button.innerHTML = buttonDefaultHTML;
            button.removeAttribute("disabled");
        });
    });
</script>
@endpush