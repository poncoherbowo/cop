@extends('layouts.app_booth')

@section('content')
<div class="container py-5">
    <h4 class="text-center">Product Gallery</h4>
    <h1 class="text-center">{{ $booth->name }}</h1>

    @if(count($products) > 0)
        <div class="row g-4 mt-4">
            @foreach($products as $product)
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="ratio ratio-1x1">
                        <div class="product" data-image="{{ $product['src'] }}" data-url="{{ $product['url'] }}" data-bs-toggle="modal" data-bs-target="#product-zoom-modal">
                            <img src="{{ $product['src'] }}" style="width: 100%; height: 100%; object-fit: contain; object-position: center;" />
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <h4 class="mt-4 text-center">Produk belum tersedia</h4>
    @endif
</div>

<div class="modal fade" id="product-zoom-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <div>
                <h5 class="modal-title text-primary">Detail Produk</h5>
            </div>
            <div>
                <a href="" id="product-zoom-url" target="_blank" type="button" class="btn btn-primary btn-sm">Detail Produk <i class="bi bi-arrow-up-right-square ms-2"></i></a>
                {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
            </div>
        </div>
        <div class="modal-body text-center">
            <div class="ratio ratio-1x1">
                <div id="product-zoom">
                    <img id="product-zoom-image" src="" style="width: 100%; height: 100%; object-fit: contain; object-position: center;" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>
@endsection

@push('js')
<script>
    const productModal = document.getElementById('product-zoom-modal')
    productModal.addEventListener('show.bs.modal', function (event) {
        const button = event.relatedTarget;
        document.getElementById('product-zoom-image').src = button.getAttribute('data-image');

        const url = button.getAttribute('data-url');
        if(url == null || url == '' || url === undefined){
            document.getElementById('product-zoom-url').style.display = 'none';
            document.getElementById('product-zoom-url').setAttribute('href', '#');
        } else {
            document.getElementById('product-zoom-url').style.display = 'inline-block';
            document.getElementById('product-zoom-url').setAttribute('href', url);
        }
    });
</script>
@endpush