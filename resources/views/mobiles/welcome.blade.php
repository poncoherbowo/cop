<!DOCTYPE html>
<html class="w-full h-full" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title'){{ config('app.name') }}</title>
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <style>
            select + .select2-container {
                width: 100% !important;
            }
        </style>

        @stack('css')
    </head>

    <body class="w-full min-h-full p-0 m-0 bg-bamboo bg-norepeat bg-cover bg-top-right">
        <div class="flex flex-col h-full justify-center items-center">
            <div class="w-full px-2">
                <div class="flex justify-between items-start py-2">
                    <div class="flex-1">
                        <img class="block h-16" src="{{ asset('img/logo-klhk.png') }}" alt="" />
                    </div>
                    <div class="flex-1 flex justify-end">
                        <img class="block h-24" src="{{ asset('img/logo-un-climate-change.png') }}" alt="" />
                    </div>
                </div>
                <div class="flex justify-center items-center pt-2 pb-2">
                    <div class="flex justify-between items-center py-2 px-2.5 rounded-3xl bg-white">
                        <a id="nav_home" class="block py-0.5 px-2 text-xs text-center rounded-2xl text-white bg-blue cursor-pointer">Home</a>
                        <a id="nav_talkshow" class="block flex-initial py-0.5 px-2 text-xs text-center cursor-pointer">Virtual Expo</a>
                        {{-- <a id="nav_vrexpo" class="block flex-initial py-0.5 px-2 text-xs text-center cursor-pointer">Virtual Expo</a>
                        <a id="nav_talkshow" class="block flex-initial py-0.5 px-2 text-xs text-center cursor-pointer">Talk Show</a> --}}
                    </div>
                </div>
            </div>
            <div id="section_content" class="pb-5 px-2">
                <div class="flex justify-center items-center pt-5 pb-2 px-2">
                    <img class="block h-32" src="{{ asset('img/logo-cop26.png') }}" alt="" />
                </div>
                <div class="pt-2 pb-2">
                    <div class="flex justify-center items-center pt-2 pb-1">
                        <h1 class="pt-2 pb-1 px-2 border-t border-white font-bold text-xs text-white text-center">COP 26 UNFCCC, GLASGOW & JAKARTA</h1>
                    </div>
                    <h2 class="font-base text-white text-2xs text-center">1 - 12 November 2021</h2>
                </div>
                <div class="pt-4 pb-2">
                    <h2 class="mb-1 font-bold text-xs text-white text-center">Leading Climate Actions Together</h2>
                    <h3 class="font-extrabold text-md text-white text-center">INDONESIA NET SINK FOLU 2030</h3>
                </div>
            </div>
            <!-- <div class="px-3 bg-bottom bg-norepeat bg-cover bg-top-right">
                <h1 class="pt-16 pb-2 font-extrabold text-xl text-blue text-center">COP 26 UNFCCC</h1>
                <h4 class="font-bold text-lg text-blue text-center">GLASGOW & JAKARTA</h4>
                <h5 class="text-blue text-md text-center">31 Oktober - 12 November 2021</h5>
                <div class="pt-4 pb-16 grid-cols-4">
                    <img class="" src="{{ asset('img/picture-gallery.png') }}" alt="" />
                </div>
            </div> -->
        </div>

        <div id="modal_vrexpo" class="modal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body p-0 m-0">
                        <video class="rounded-sm" width="100%" height="100%" controls>
                            <source src="{{ asset('img/virtual-expo.mp4') }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_talkshow" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body p-0 m-0">
                        <div class="w-full p-4 rounded-sm bg-blue">
                            <form action="{{ route('login') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="pb-1.5">
                                    <label class="form-label font-extrabold text-sm text-white">Email</label>
                                    <input class="form-control" name="email" id="email" type="text" required="required" placeholder="Input your email" />
                                    <div class="invalid-feedback" id="invalid-feedback-email"></div>
                                </div>
                                <div class="pt-1.5 pb-1.5" style="display: none;">
                                    <label class="form-label font-extrabold text-sm text-white">Password</label>
                                    <input class="form-control" name="password" id="password" type="password" required="required" placeholder="Input your password" value="copv2021"/>
                                    <div class="invalid-feedback" id="invalid-feedback-password"></div>
                                </div>
                                <div class="mt-3">
                                    <button class="w-full py-2 px-2 rounded-md bg-green font-extrabold text-sm text-white" type="button" id="btnLogin">Login</button>
                                </div>
                                <div class="pt-5">
                                    <a class="block py-2 px-2 rounded-md border border-white font-extrabold text-sm text-white text-center cursor-pointer" data-bs-target="#modal_register" data-bs-toggle="modal" data-bs-dismiss="modal">Register New Account</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_register" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body p-0 m-0">
                        <div class="w-full p-4 rounded-sm bg-blue">
                            <form action="{{ route('register') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="pb-1.5">
                                    <label class="form-label font-extrabold text-sm text-white">Fullname</label>
                                    <input class="form-control" name="name" id="name" type="text" required="required" placeholder="Input your fullname" />
                                    <div class="invalid-feedback" id="invalid-feedback-name"></div>
                                </div>
                                <div class="pb-1.5">
                                    <label class="form-label font-extrabold text-sm text-white">Email</label>
                                    <input class="form-control" name="email" id="emailRegist" type="text" required="required" placeholder="Input your email" />
                                    <div class="invalid-feedback" id="invalid-feedback-emailRegist"></div>
                                </div>
                                <div class="pt-1.5 pb-1.5" style="display: none;">
                                    <label class="form-label font-extrabold text-sm text-white">Password</label>
                                    <input class="form-control" name="password" id="passwordRegist" type="password" required="required" placeholder="Input your password" value="copv2021"/>
                                    <div class="invalid-feedback" id="invalid-feedback-passwordRegist"></div>
                                </div>
                                <div class="pt-1.5 pb-1.5" style="display: none;">
                                    <label class="form-label font-extrabold text-sm text-white">Password Confirmation</label>
                                    <input class="form-control" name="password_confirmation" id="password_confirmationRegist" type="password" required="required" placeholder="Input your password" value="copv2021"/>
                                    <div class="invalid-feedback" id="invalid-feedback-password_confirmationRegist"></div>
                                </div>
                                <div class="pb-1.5">
                                    <label class="form-label font-extrabold text-sm text-white">Phone Number</label>
                                    <input class="form-control" name="phone" id="phone" type="text" required="required" placeholder="Input your phone number" />
                                    <div class="invalid-feedback" id="invalid-feedback-phone"></div>
                                </div>
                                <div class="pb-1.5">
                                    <label class="form-label font-extrabold text-sm text-white">Country</label>
                                    <br>
                                    <select class="select2 form-control" name="country" id="country">
                                        <option value="">- Select Country -</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Åland Islands">Åland Islands</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guernsey">Guernsey</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Isle of Man">Isle of Man</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jersey">Jersey</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                        <option value="Korea, Republic of">Korea, Republic of</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montenegro">Montenegro</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau">Palau</option>
                                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Helena">Saint Helena</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan">Taiwan</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-leste">Timor-leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                    <div class="invalid-feedback" id="invalid-feedback-country"></div>
                                </div>
                                <div class="mt-3">
                                    <button class="w-full py-2 px-2 rounded-md bg-green font-extrabold text-sm text-white" type="button" id="btnRegister">Register</button>
                                </div>
                                <div class="pt-5">
                                    <a id="nav_register" class="block py-2 px-2 rounded-md font-extrabold text-sm text-white text-center bg-red cursor-pointer" data-bs-dismiss="modal">Close</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <script>
            $(".select2").select2({
                dropdownParent: $("#modal_register")
            });

            $("#btnLogin").click(function() {

                $(".form-control").removeClass(" is-invalid");

                var email = $("#email").val();
                var password = $("#password").val();

                var data = {
                    '_token': '{{ csrf_token() }}',
                    'email': email,
                    'password': password,
                };

                // console.log(data)

                $.ajax({
                    type: "POST",
                    url: '/login',
                    data: data,

                    beforeSend: function() {

                    },
                    success: function(data) {
                        window.location.href = '/home';
                    },
                    error: function(jqXHR, exception) {
                        // console.log(jqXHR.responseText);
                        // console.log(exception);

                        var resJson = JSON.parse(jqXHR.responseText);

                        if(resJson.hasOwnProperty('errors')){
                            const entries = Object.entries(resJson.errors)
                            // console.log(entries)
                            for (const [key, val] of entries) {
                                // console.log(`Key ${key} => ${val}s`)
                                $("#"+key).addClass(" is-invalid");
                                $("#invalid-feedback-"+key).empty();
                                $("#invalid-feedback-"+key).text(val);
                            }
                        }
                    }
                })
            })

            $("#btnRegister").click(function() {

                    $(".form-control").removeClass(" is-invalid");

                    var name = $("#name").val();
                    var email = $("#emailRegist").val();
                    var phone = $("#phone").val();
                    var password = $("#passwordRegist").val();
                    var password_confirmation = $("#password_confirmationRegist").val();
                    var country = $("#country").val();

                    var data = {
                        '_token': '{{ csrf_token() }}',
                        'name': name,
                        'email': email,
                        'phone': phone,
                        'password': password,
                        'password_confirmation': password_confirmation,
                        'country': country,
                    };

                    // console.log(data)

                    $.ajax({
                        type: "POST",
                        url: '/register',
                        data: data,

                        beforeSend: function() {

                        },
                        success: function(data) {
                            window.location.href = '/home';
                        },
                        error: function(jqXHR, exception) {
                            // console.log(jqXHR.responseText);
                            // console.log(exception);

                            var resJson = JSON.parse(jqXHR.responseText);

                            if(resJson.hasOwnProperty('errors')){
                                const entries = Object.entries(resJson.errors)
                                // console.log(entries)
                                for (const [key, val] of entries) {
                                    // console.log(`Key ${key} => ${val}s`)
                                    var newKey = ((key == 'email' || key == 'password' || key == 'password_confirmation') ? key+'Regist' : key);
                                    $("#"+newKey).addClass(" is-invalid");
                                    $("#invalid-feedback-"+newKey).empty();
                                    $("#invalid-feedback-"+newKey).text(val);
                                }
                            }
                        }
                    })
                })
        
        </script>
            
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app-web.js') }}"></script>
        @stack('js')
    </body>
</html>