require('./bootstrap');
var Turbolinks = require("turbolinks")
Turbolinks.start()
document.addEventListener('turbolinks:load', () => {
	window.livewire.rescan()
})

$(function() {
	var modalVrExpo 	= new bootstrap.Modal(document.getElementById('modal_vrexpo'));
	var modalTalkShow 	= new bootstrap.Modal(document.getElementById('modal_talkshow'));

	$('#nav_home').on('click', function() {
		$('#nav_talkshow').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_vrexpo').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_home').addClass('rounded-2xl text-white bg-blue');

		modalVrExpo.hide();
		modalTalkShow.hide();
	});

	$('#nav_vrexpo').on('click', function() {
		$('#nav_home').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_talkshow').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_vrexpo').addClass('rounded-2xl text-white bg-blue');
		modalVrExpo.show();
	});

	$('#nav_talkshow').on('click', function() {
		$('#nav_vrexpo').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_home').removeClass('rounded-2xl text-white bg-blue');
		$('#nav_talkshow').addClass('rounded-2xl text-white bg-blue');
		modalTalkShow.show();
	});

});