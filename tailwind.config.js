module.exports = {
    purge: [
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue'
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        colors: {
            white: '#ffffff',
            green: '#8cdc73',
            blue: '#37328c',
            red: '#e3292b'
        },
        fontSize: {
            '2xs': '.7rem',
            '3xs': '.65rem',
            '4xs': '.6rem',
            'xs': '.75rem',
            'sm': '.875rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5rem',
        },
        extend: {
            backgroundImage: {
                'bamboo': "url('/img/bg-bamboo.jpg')",
                'bottom': "url('/img/bg-bottom.png')"
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
